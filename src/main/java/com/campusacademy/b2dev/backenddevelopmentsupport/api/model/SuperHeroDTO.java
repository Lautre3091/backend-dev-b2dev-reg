package com.campusacademy.b2dev.backenddevelopmentsupport.api.model;

import com.campusacademy.b2dev.backenddevelopmentsupport.model.Vilain;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SuperHeroDTO extends SuperHeroMinimalDTO {
    String secretIdentity;
    Long nemesisId;
    SuperHeroMinimalDTO mentor;
    List<SuperHeroMinimalDTO> sideKicks;
    List<PowerMinimalDTO> powers;

    public SuperHeroDTO(
            Long id,
            String superHeroName,
            String secretIdentity,
            Vilain nemesis,
            SuperHeroMinimalDTO mentor,
            List<SuperHeroMinimalDTO> sideKicks,
            List<PowerMinimalDTO> powers
    ) {
        super(id, superHeroName);
        this.secretIdentity = secretIdentity;
        this.mentor = mentor;
        this.sideKicks = sideKicks;
        this.powers = powers;

        if (nemesis != null) {
            this.nemesisId = nemesis.getId();
        }
    }
}
