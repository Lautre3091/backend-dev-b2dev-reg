package com.campusacademy.b2dev.backenddevelopmentsupport.api;

import com.campusacademy.b2dev.backenddevelopmentsupport.api.mapper.UserMapper;
import com.campusacademy.b2dev.backenddevelopmentsupport.api.model.UserDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, value = "/me")
@RequiredArgsConstructor
public class ProfileController {

    private final UserMapper mapper;

    @GetMapping
    public ResponseEntity<UserDTO> me(HttpServletRequest request) {
        return ResponseEntity.ok(mapper.map(request.getUserPrincipal()));
    }
}
