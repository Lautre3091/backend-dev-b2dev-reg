package com.campusacademy.b2dev.backenddevelopmentsupport.model;

public enum SuperHerosType {
    VOLANT(true),
    SANS_POUVOIR(false);

    private boolean canFly;

    SuperHerosType(boolean canFly) {
        this.canFly = canFly;
    }

    boolean getCanFly() {
        return this.canFly;
    }

}